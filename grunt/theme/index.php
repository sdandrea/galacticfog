<?php get_header(); ?>


  <div id="panel-1" class="panel clearfix">
    
    <div id="hero">
      <div class="spaceship"></div>
      <div id="type">
        <h1 class="lovelo_black"> <?php bloginfo( 'name' ); ?>  </h1>
        <h4 class="white">
          <?php bloginfo( 'description' ); ?>
        </h4>
      </div>
    </div>
  
    <div class="panel-image-1 panel-image" >
      <div class="full"></div>
    </div>

    <div id="scroll">
      <img src="images/scroll.png">
    </div>
  </div>

  <div id="panel-2" class="panel clearfix">

    <div class="row clearfix">
      <div class="block fl-left">
        <?php 
          query_posts( array( 'name' => 'meet-gestalt')); 
          if (have_posts()): while (have_posts()) : the_post();
        ?>
          <h2>[ <?php the_title(); ?> ]</h2>
          <?php the_content(); ?>
        <?php  endwhile;  endif;  wp_reset_query(); ?>
        <a href="#" class="btn white">LEARN MORE</a>
      </div>


      <div class="block fl-right">
        <div class="illustration">
          <img src="images/robot1.svg">
        </div> 
      </div>
    </div>
    

    <div class="full">
      <div class="rotate"></div>
    </div>

  </div>

  <div id="panel-3" class="panel clearfix">

    <div class="row row-1 clearfix">
      <div class="block fl-left pad-r">
        <span class="illustration" data-bottom-top="transform:translateY(-100px);opacity:0" data-center-top="transform:translateY(0px);opacity:1">
          <img src="images/ill1.png">
        </span> 
      </div>
      <div class="block fl-right pad-l"  data-bottom-top="transform:translateY(100px);opacity:.8" data-center-center="transform:translateY(0);opacity:1">
        <h2>[ WHY CHOOSE US? ]</h2>
        <?php 
          query_posts( array( 'name' => 'streamline-integration')); 
          if (have_posts()): while (have_posts()) : the_post();
        ?>
          <h3>[ <?php the_title(); ?> ]</h3>
          <?php the_content(); ?>
        <?php  endwhile;  endif;  wp_reset_query(); ?>
      </div>
    </div>

    <div class="row row-2 clearfix">
      <div class="block fl-left pad-r">
        <span class="illustration"  data-bottom-center="transform:translate(-100px,100px);opacity:0" data-center-top="transform:translate(0,0);opacity:1">
          <img src="images/ill2.png">
        </span> 
      </div>
      <div class="block fl-right pad-l"  data-bottom-top"transform:translateY(100px);opacity:.8" data-center-center="transform:translateY(0);opacity:1">
        <?php 
          query_posts( array( 'name' => 'scalability')); 
          if (have_posts()): while (have_posts()) : the_post();
        ?>
          <h3>[ <?php the_title(); ?> ]</h3>
          <?php the_content(); ?>
        <?php  endwhile;  endif;  wp_reset_query(); ?>   
      </div>
    </div>

    <div class="row row-3 clearfix">
      <div class="block fl-left pad-r" >
        <span class="illustration"  data-bottom-top="transform:translate(100px,-100px);opacity:0" data-top-center="transform:translate(0,0);opacity:1">
          <img src="images/ill3.png">
        </span> 
      </div>
      <div class="block fl-right pad-l" data-bottom-top="transform:translateY(100px);opacity:.8" data-center-center="transform:translateY(0);opacity:1">
        <?php 
          query_posts( array( 'name' => 'future-proof-apps')); 
          if (have_posts()): while (have_posts()) : the_post();
        ?>
          <h3>[ <?php the_title(); ?> ]</h3>
          <?php the_content(); ?>
        <?php  endwhile;  endif;  wp_reset_query(); ?>   
      </div>
    </div>
  </div>
  
  <div id="panel-4" class="panel clearfix">
    <div class="row clearfix">
      <div class="block fl-left" data-bottom-top="transform:translateY(50px);opacity:.8" data-top-top="transform:translateY(0);opacity:1">
        <?php 
          query_posts( array( 'name' => 'cross-integrated-microservices')); 
          if (have_posts()): while (have_posts()) : the_post();
        ?>
          <h3>[ <?php the_title(); ?> ]</h3>
          <?php the_content(); ?>
        <?php  endwhile;  endif;  wp_reset_query(); ?>   
      </div>
      <div class="block fl-right">
        <span class="illustration" data-bottom-top="transform:translateY(-200px);opacity:0" data-bottom-bottom="transform:translateY(0px);opacity:1">
          <img src="images/ill6.png">
        </span> 
      </div>
    </div>
  </div>

  <div id="panel-5" class="panel clearfix">

    <div class="row clearfix">
      <div class="block fl-left">
        <span class="illustration" data-bottom-top="transform:translateY(-100px);opacity:0" data-bottom-bottom="transform:translateY(0px);opacity:1">
          <img src="images/ill5.png">
        </span>
      </div>
      <div class="block fl-right" data-bottom-top="transform:translateY(100px);opacity:.8" data-top-top="transform:translateY(0);opacity:1">
        <?php 
          query_posts( array( 'name' => 'build-cloud-native-apps')); 
          if (have_posts()): while (have_posts()) : the_post();
        ?>
          <h3>[ <?php the_title(); ?> ]</h3>
          <?php the_content(); ?>
        <?php  endwhile;  endif;  wp_reset_query(); ?> 
      </div>
    </div>

    <svg class="clip-svg" width="0" height="0">
      <defs>
        <clipPath id="clip-shape" clipPathUnits="objectBoundingBox">
          <polygon points="0 0, 0 100, 100 100"/>
        </clipPath>
      </defs>
    </svg>
    
    <div id="bottom"></div>
  </div>


<?php get_footer(); ?>