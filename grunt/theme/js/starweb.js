gf.starWeb = (function() {
  var starsLayer = new Layer();
  var webLayer = new Layer();

  var ratio =  window.devicePixelRatio > 1 ? 2 : 1;

  var webCount =  ( ( $(window).width() * $(window).height()) / 70000 ) * ratio ;
  var starsCount = ( ( $(window).innerWidth() * $(window).innerHeight () ) / 4000 ) * ratio ;

  // set canvas size, doubled for retina
  if ( window.retina || window.devicePixelRatio > 1) {
    view.setViewSize( gf.util.ww()*2,  gf.util.wh()*2);
    // set canvas size in css
    $('canvas').css("width", gf.util.ww()+"px").css("height", gf.util.wh()+"px");
    console.log(  window.devicePixelRatio );
    if ( window.devicePixelRatio > 1 ) {
        var ctx = $('canvas')[0].getContext('2d');
        ctx.scale(2,2);               
    }
  };


  var drawWeb = function() {
    webLayer = new Layer();
    var paths = [];
    
      // The maximum height of the wave:
      var height = view.size;
      var width = view.size;
      var paths = [];

      // Create a new path and style it:
      var path = new Path({
        strokeColor: '#008888',
        // strokeColor: '#ffffff',
        opacity : .1,
        strokeWidth: 2 / ratio,
        strokeCap: 'square',
        strokeJoin: 'bevel'

      });

      paths.push(path.segments);

      var a = [];
      for (var i = 0; i <= webCount; i++) {
        // a.push(Point.random() * view.size);
        var x = Math.floor(Math.random() * ( view.size._width  + 100 ) ) + 1 
        var y = Math.floor(Math.random() * ( view.size._height + 100 ) ) + 1 

        a.push( new Point( x , y ) );
      };

      for( var i = 0; i <= webCount; i++ ){

        if( i  === webCount ){ 
          path.add( a[0] ) ;
        }else{
          path.add( a[i] ) ;
        }
      }

      var api = {
        'path' : path
      }

      return api;

  }

  var drawStars = function() {
      starsLayer.activate();
      // Create a symbol, which we will use to place instances of later:
      var circlePath = new Shape.Circle({
          center: new Point(0, 0),
          radius: 1.8 / ratio ,
          fillColor: 'white',
          opacity: .5
      });

      var symbol = new Symbol(circlePath);

      // Place the instances of the symbol:
      for (var i = 0; i < starsCount; i++) {
          // The center position is a random point in the view:
          var center = Point.random() * view.size;
          var placed = symbol.place(center);
          var scale = (i + 1) / starsCount * .9;
          
          placed.scale(scale);

          placed.data.vector = new Point({
              angle: 0,
              length : scale * Math.random() * .2
          });
      }

  }


  function keepInView(item) {
      var position = item.position;
      var itemBounds = item.bounds;
      var bounds = view.bounds;
      if (itemBounds.left > bounds.width) {
          position.x = -item.bounds.width;
      }

      if (position.x < -itemBounds.width) {
          position.x = bounds.width + itemBounds.width;
      }

      if (itemBounds.top > view.size.height) {
          position.y = -itemBounds.height;
      }

      if (position.y < -itemBounds.height) {
          position.y = bounds.height  + itemBounds.height / 2;
      }
  }

  var vector = new Point({
    angle: 45,
    length: 0
  });

  var mouseVector = vector.clone();

  document.onmousemove =  function handleMouseMove(event) {
    mouseVector = view.center - {x : event.x, y : event.y}
  }

  function animateStars(event) {
    starsLayer.activate();
    /* 
      How fast do you want the motion to respond to your mouse
      Lower the number the more responsive
    */
    var tracking = 3;
    vector = vector + (mouseVector - vector) / tracking;
    /* stars */
    for (var i = 0; i < starsCount; i++) {
        var item = project.activeLayer.children[i];
        var size = item.bounds.size;
        /* 
          How fast do you the stars to move
          Lower the number the faster
        */
        view_speed = 50;
        var length = vector.length / view_speed * size.width / view_speed ;
        item.position += vector.normalize(length) + item.data.vector;
        keepInView(item);
    }
  }
  
  function animateWeb(event) {
    webLayer.activate();
    for (var i = 0; i < web.path.segments.length ; i++) {
      var segment = web.path.segments[i];
      // A cylic value between -1 and 1
      var sin = Math.cos(event.time * .015 + i);
      // Change the y position of the segment point:
      segment.point.y = sin * $(window).height();
    }
  };

  var stars = drawStars(); 
  var web = drawWeb();


  return {
    'animateStars'  : animateStars,
    'animateWeb'    : animateWeb
  }

})();

function onFrame(event) {
  gf.starWeb.animateStars(event);
  gf.starWeb.animateWeb(event);
}





