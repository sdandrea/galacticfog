gf.util = {

  ww : function(){ 
    return $(window).width();
  },
  wh : function (){
    return $(window).height();
  }

}

var s = skrollr.init({
  easing: {
    wtf: Math.random,
    inverted: function(p) {
        return 1 - p;
    }
  },
  beforerender: function(data) {
    return data.curTop > data.lastTop;
  }
}); 

gf.isMobile = s.isMobile();

if( s.isMobile() ){ 
  s.destroy(); 
}else{
  $('.home').height( $('.home').height() - 365 )
}

$('li.nav-contact a').on('click', function(e){
  if( !gf.isMobile ){
    e.preventDefault();

    $('.contact-form').toggleClass('open');
    $('.contact-form').css({
      'position' : 'relative'
    });

    var to = setTimeout(function() {
      if( !$('.contact-form').hasClass('open') ){
        $('.contact-form').css({
          'position' : 'absolute'
        });
      }
      lock = false;
    }, 750);
  }
});

 
