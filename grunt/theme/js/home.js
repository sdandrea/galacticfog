gf.home = (function(){

  var $hero = $('#hero');

  var w   = gf.util.ww() / 2 - $hero.outerWidth() / 2 + 60;
  var h   = gf.util.wh() / 2 - $hero.outerHeight() / 2 - $('.spaceship').outerHeight() / 4;
  var sw  = gf.util.ww() / 2 - $('#scroll').outerWidth() / 2;

  h = gf.isMobile ? h + $('nav').outerHeight() : h ;
  w = gf.isMobile ? w - 60 : w ;

  $("#panel-1").css({
    "height" :  gf.util.wh()
  });

  $('#hero').css({
    "opacity" : 0,
     "-webkit-transform": "translate(" + w + "px, " + gf.util.wh() / 2 + "px)",
        "-moz-transform": "translate(" + w + "px, " + gf.util.wh() / 2 + "px)",
             "transform": "translate(" + w + "px, " + gf.util.wh() / 2 + "px)",
  });

  $("#scroll").css({
    'opacity' : 1,
    "-webkit-transform": "translate(" + sw + "px," +  gf.util.wh() + "px)",
       "-moz-transform": "translate(" + sw + "px," +  gf.util.wh() + "px)",
            "transform": "translate(" + sw + "px," +  gf.util.wh() + "px)",
    "-webkit-transition": "none",
       "-moz-transition": "none",
            "transition": "none",
    "-webkit-transition-delay": ".05s",
       "-moz-transition-delay": ".05s",
            "transition-delay": ".05s"
  });

  setTimeout(function() {
    $("#hero").css({
        "opacity" : 1,
        "-webkit-transform": "translate(" + w + "px," + h + "px)",
           "-moz-transform": "translate(" + w + "px," + h + "px)",
                "transform": "translate(" + w + "px," + h + "px)",
        "-webkit-transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
        "-moz-transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
        "transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
    });

    $("#scroll").css({
      "opacity" : 1,
      "-webkit-transform": "translate(" + sw + "px, 0px)",
         "-moz-transform": "translate(" + sw + "px, 0px)",
      "-webkit-transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
      "-moz-transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
      "transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
    });

    var add;
    if( gf.util.ww() < 1200 ){
      add = 0.4
    }else if( gf.util.ww() < 1800 ){
      add = 0.5
    }else if( gf.util.ww() > 1800 ) {
      add = 0.6
    }

    // var height = ( $('#panel-4').outerHeight() + $('#panel-5').outerHeight() + ( gf.util.wh() * add) );

    // $("#bottom").css({
    //   "height" : height
    // });

  }, 1)

  $(function() {


    $(window).on("resize" , function() { 
        w   = gf.util.ww() / 2 - $hero.outerWidth() / 2;
        h   = gf.util.wh() / 2 - $hero.outerHeight() / 2 - $('.spaceship').outerHeight() / 4;
        sw  = gf.util.ww() / 2 - $("#scroll").outerWidth() / 2;

        h = gf.isMobile ? h + $('nav').outerHeight() + 60 : h ;

        $("#hero").css({
            "opacity" : 1,
            "-webkit-transform": "translate(" + w + "px," + h + "px)",
               "-moz-transform": "translate(" + w + "px," + h + "px)",
                    "transform": "translate(" + w + "px," + h + "px)",
            "-webkit-transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
            "-moz-transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
            "transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
        });

        $("#scroll").css({
          "opacity" : 1,
          "-webkit-transform": "translate(" + sw + "px, 0px)",
             "-moz-transform": "translate(" + sw + "px, 0px)",
          "-webkit-transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
          "-moz-transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
          "transition":"all 1s cubic-bezier(0.19, 1, 0.22, 1)",
        });

        $("#panel-1").css({
          "height" :  gf.util.wh()
        });


        var add;
        if( gf.util.ww() < 1200 ){
          add = 0.4
        }else if( gf.util.ww() < 1800 ){
          add = 0.5
        }else if( gf.util.ww() > 1800 ) {
          add = 0.6
        }

        if(!gf.isMobile){
          setTimeout(function() {
            $("body").css({
              "height" : $("body").height() - 600
            });
          }, 40);
        }

    });
  

  })

})();