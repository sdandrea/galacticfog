<?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <div class="row clearfix">
      <div class="block block-left">
        <h4> 
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php the_title(); ?> 
            <span> Read More > </span>
        </h4>

      </div>
      <div class="block block-right">
        <?php the_excerpt(); ?>
      </div>
      
    </div>  
    
<?php endwhile; ?>
<?php endif; ?>
