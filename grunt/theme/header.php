<!doctype html>
<html>
  <head>
    
    <meta charset="utf-8">
    
    <title><?php bloginfo('name'); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <script type="text/javascript">
      var gf = {};
    </script>

    <?php wp_head(); ?>
  
  </head>

  <?php 
    
    $bodyId= '';
    $bodyClass= '';
    $echoTitle = false;
  
    if ( is_home() ) {

      $bodyId = $bodyClass  ='home';
      
    }else if ( is_category() ) {

      $bodyId = 'category';
      $bodyClass = "category-" . strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', single_cat_title( '', false ) )) ;
      $echoTitle = true;
      $title =  single_cat_title( '', false );

    }else if(is_page() ){    

      $bodyId = 'page';            
      $bodyClass = "page-" . strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', get_the_title( '', false ) )) ;
      // $echoTitle = true;
      $title =  get_the_title( '', false );

    }else if(is_single() ){    
      $bodyId = 'single';
      $bodyClass = "single-" . strtolower( preg_replace('/[^A-Za-z0-9\-]/', '', get_the_title( '', false ) )) ;
      $title  = get_the_title( '', false );

    }

  ?>

  <body id="<?php echo $bodyId ?>" class="<?php echo $bodyClass ?>">
    <div id="wrapper">
      <div class="content">
        <nav>            

            <div id="logo">
              <a class="icon" href="/"></a>
              <h2  class="<?php echo $echoTitle ? 'multi-line' : 'single-line' ?>" >
                <a href="/"> Galactic Fog </a>
                <?php if( $echoTitle ) { echo "<span> [ $title ] </span>"; }; ?> 
              </h2>
            </div>

            <div class="nav-links">
            <ul>
                  <li class="nav-learn"><a href="/learn">Learn</a></li>
                  <li class="nav-blog"><a href="/blog">Blog</a></li>
                  <li class="nav-contact"><a href="/contact">Contact</a></li>
                </ul>

            </div>
        </nav>  
        <?php if($bodyClass != 'page-contact'): ?>
        <div class="contact-form">
          <?php 
            $args = array(
              'pagename' => 'contact'
            );
            query_posts( $args ); 
            if (have_posts()): while (have_posts()) : the_post();
          ?>
          <div class="row">
            <?php the_content(); ?>
          </div>
          <?php 
            endwhile; 
            endif;  
            wp_reset_query();
          ?>
        </div>
      <?php  endif;  ?>

