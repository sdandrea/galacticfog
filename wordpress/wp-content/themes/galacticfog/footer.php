    <footer>
      <div class="row">
        <div class="clearfix">
          <h3>[ Build Future Proof Applications ]</h3>
          <a class="btn white"> GET STARTED TODAY </a>
        </div>
        <div class="clearfix">
          
          <ul>

            <li class="header"> Galactic Fog </li>
            <li>
              <a href="#">About</a>
            </li>
            <li>
              <a href="#">Blog</a>
            </li>
            <li>
              <a href="#">Contact</a>
            </li>
          </ul>

          <ul>

            <li class="header"> Product </li>
            <li>
              <a href="#">Learn</a>
            </li>
          </ul>


          <ul>
            <li class="header">
              Connect
            </li>
            <li>
              <a>Twitter</a>
            </li>
            <li>
              <a>Facebook</a>
            </li>
            <li>
              <a>Instagram</a>
            </li>
          </ul>

      
      </div>
    </footer>
  </div> 
</div>


    <?php wp_footer(); ?>
  <canvas id="canvas" resize hidpi="on"></canvas>
  <script src="<?php bloginfo('template_url'); ?>/js/starweb.js" type="text/paperscript"  canvas="canvas"></script>
  



</body>
</html>
